<?php

require_once ($_SERVER['DOCUMENT_ROOT']) . '/interfaces/EventListenerInterface.php';
require_once ($_SERVER['DOCUMENT_ROOT']) . '/interfaces/LoggerInterface.php';

abstract class Storage implements LoggerInterface, EventListenerInterface
{
    abstract function create (TelegraphText $object);
    abstract function read ($objectName);
    abstract function update ($objectName, $newObject);
    abstract function delete ($objectName);
    abstract function list ();

    const SEPARATOR_LOG = '-->';

    public function logMessage(string $errorText):string
    {
        $date = date('d-m-y, h:i:s');
        $message =  self::SEPARATOR_LOG . $date.' '.$errorText . PHP_EOL;
        file_put_contents('log.txt', $message, FILE_APPEND);
    }

    public function lastMessages(int $quantityError):array
    {
        $this->checkEvent('lastMessages');
        $getError = file_get_contents('log.txt');
        $arrError = array_reverse(explode(self::SEPARATOR_LOG, $getError));
        return array_slice($arrError, 0, $quantityError);
    }

    public array $methods = [];

    public function attachEvent($method, $callBackFunc)
    {
        $this->methods[$method] = $callBackFunc;
    }

    public function detouchEvent($method)
    {
        unset($this->methods);
    }

    public function showEvents()
    {
        var_dump($this->methods);
    }

    public function checkEvent($method)
    {
        if (isset($this->methods[$method])){
            call_user_func($this->methods[$method]);
        }
    }
}