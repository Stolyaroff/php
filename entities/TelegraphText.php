<?php

class TelegraphText
{
    private string $title;
    private string $text;
    private string $author;
    private string $published;
    private string $slug;

    public function setAuthor($author)
    {
        if (strlen($author) < 120)
        {
            $this->author = $author;
        } else {
            echo 'Слишком длинное имя';
        }
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function setSlug($slug)
    {
        if (preg_match('/[a-zA-Z1-9-_.]+$/', $slug)){
            $this->slug = $slug;
        } else {
            echo 'Название может содержать только латинские буквы, цифры и символы _-.';
        }
    }

    public function getSlug ()
    {
        return $this->slug;
    }

    public function setPublished($published)
    {
        if ($published == NULL){
            $this->published = date('d.m.y');
        }
        if (strtotime($published) >= strtotime(date('d.m.y'))) {
            $this->published = $published;
        }
    }

    public function getPublished()
    {
        return $this->published;
    }

    public function __set(string $name, $value): void
    {
        if ($name = 'author'){
            $this->setAuthor($value);
        }
        if ($name = 'slug'){
            $this->setSlug($value);
        }
        if ($name = 'published'){
            $this->setPublished($value);
        }
    }

    public function __get(string $name)
    {
        if ($name = 'author'){
            return $this->getAuthor();
        }
        if ($name = 'slug'){
            return $this->getSlug();
        }
        if ($name = 'published'){
            return $this->getPublished();
        }
        if ($name = 'text'){
            return $this->loadText();
        }
    }

    public function __construct(string $author, string $slug, $published = NULL)
    {
        $this->setAuthor($author);
        $this->setSlug($slug);
        $this->setPublished($published);
    }

    /**
     * @throws Exception
     */
    public function storeText(string $text, string $title)
    {
        if (strlen($text) < 2 || strlen($text) > 4 || strlen($text) == null){
            function myException($e) {
                echo "<div style='font-weight: bold; background: pink; width: 200px; height: 200px'>$e</div> ";
            }
            set_exception_handler('myException');

            throw new Exception('Длинна строки должна быть от 1 до 500 символов');
        }
        $this->text = $text;
        $this->title = $title;

//        if (!file_exists($this->slug) || file_get_contents($this->slug) == NULL){
//            $slug = $this->slug . '.txt';
//            $array = [
//                'text' => $text,
//                'title' => $title,
//                'author' => $this->author,
//                'published' => $this->published,
//            ];
//            file_put_contents($slug, serialize($array));
//        }else{
//            echo 'такой файл существует';
//        }
    }

    public function loadText():array
    {
        $text = unserialize(file_get_contents($this->slug . '.txt'));
        $this->title = $text['title'];
        $this->text = $text['text'];
        $this->author = $text['author'];
        $this->published = $text['published'];
        return $text;
    }

    public function editText(string $text, string $title)
    {
        if (file_exists($this->slug)){
            $file = unserialize(file_get_contents($this->slug));
            $file['title'] = $title;
            $file['text'] = $text;
            file_put_contents($this->slug, serialize($file));
        }else{
            echo 'файл отсутствует';
        }
    }
}