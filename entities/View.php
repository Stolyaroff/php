<?php

abstract class View
{
    public $storage;
    function __construct($storage)
    {
        $this->storage = $storage;
    }

    abstract function displayTextById ($id);
    abstract function displayTextByUrl ($url);
    abstract function update ($objectName, $newObject);
    abstract function delete ($objectName);
    abstract function list ();
}