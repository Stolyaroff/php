<?php



class FileStorage extends Storage {
    const FILE_FORMAT = '.txt';

    public function create(TelegraphText $object)
    {
        $this->checkEvent('create');

        $fileName = $object->slug . '_' . date("d.m.y") . self::FILE_FORMAT;
        if (file_exists($fileName)){
            $num = 1;
            $fileName = $object->slug . '_' . date("d.m.y") . '_' . $num . self::FILE_FORMAT;
            while (file_exists($fileName)){
                $searchNum = '/\d+(?=\D*$)/';
                preg_match($searchNum, $fileName,$matches);
                $matches[0] += 1;
                $fileName = preg_replace($searchNum, $matches[0], $fileName);
            }
        }
        $slug = preg_replace('/'.self::FILE_FORMAT.'/', '', $fileName);
        $object->slug = $slug;
        $fileContent = serialize($object);
        file_put_contents ($fileName, $fileContent);
        return $slug;
    }

    public function read($objectName)
    {
        $this->checkEvent('read');

        if (file_exists($objectName . self::FILE_FORMAT)){
            return (unserialize(file_get_contents($objectName . self::FILE_FORMAT)));
        } else {
            echo 'Такого файла не существует';
        }
    }

    public function update($objectName, $newObject)
    {
        $this->checkEvent('update');

        if (file_exists($objectName . self::FILE_FORMAT) && file_exists($newObject . self::FILE_FORMAT)){
            file_put_contents($objectName, file_get_contents($newObject . self::FILE_FORMAT));
        } else {
            echo 'Такого файла не существует';
        }
    }

    public function delete($objectName)
    {
        $this->checkEvent('delete');

        if (file_exists($objectName . self::FILE_FORMAT)){
            unlink($objectName . self::FILE_FORMAT);
        } else {
            echo 'Такого файла не существует';
        }
    }

    public function list()
    {
        $this->checkEvent('list');

        $dirFiles = scandir(__DIR__);
        foreach ($dirFiles as $file){
            if (stripos($file, self::FILE_FORMAT)){
                echo '<pre>';
                var_dump(unserialize(file_get_contents($file)));
            }
        }
    }
}