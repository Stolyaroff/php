<?php

require_once ($_SERVER['DOCUMENT_ROOT']) . '/interfaces/EventListenerInterface.php';

abstract class User implements EventListenerInterface
{
    protected $id;
    protected $name;
    protected $role;

    abstract function getTextsToEdit();

    protected array $methods = [];

    public function attachEvent($method, $callBackFunc)
    {
        $this->methods[$method] = $callBackFunc;
    }

    public function detouchEvent($method)
    {
        unset($this->methods);
    }
    public function checkEvent($method)
    {
        if (isset($this->methods[$method])){
            call_user_func($this->methods[$method]);
        }
    }
}