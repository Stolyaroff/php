<?php

interface EventListenerInterface
{
    public function attachEvent($method, $callBackFunc);
    public function detouchEvent($method);
}