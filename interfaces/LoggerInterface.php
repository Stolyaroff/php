<?php

interface LoggerInterface
{
    public function logMessage(string $errorText);
    public function lastMessages(int $quantityError);
}