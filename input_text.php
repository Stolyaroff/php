<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

require_once $_SERVER['DOCUMENT_ROOT'] . '/autoload.php';

    $mailMessage = '';

    if (isset($_POST['submit']) && !empty($_POST['author']))
    {
        $newTelegraph = new TelegraphText($_POST['text'], $_POST['author']);
        try {
            $newTelegraph->storeText($_POST['text'], $_POST['author']);
        } catch (myException $e) {
            echo $e->getMessage();
        }

        $newFile = new FileStorage();
        $newFile->create($newTelegraph);

        $mail = new PHPMailer(true);

        try {
            //Server settings
            $mail->SMTPDebug = SMTP::DEBUG_OFF;                         //Enable verbose debug output
            $mail->isSMTP();                                            //Send using SMTP
            $mail->Host       = 'smtp.mail.ru';                         //Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
            $mail->Username   = '';
            $mail->Password   = '';
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
            $mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

            //Recipients
            $mail->setFrom('stolyar.off@mail.ru', 'Telegraph');
            $mail->addAddress($_POST['email']);

            //Content
            $mail->isHTML(true);
            $mail->Subject = 'Your telegraph';
            $mail->Body    = $_POST['text'];

            $mail->send();
            $mailMessage = 'Письмо отправлено';
            $mailSend = true;
        } catch (Exception $e) {
            $mailSend = false;
            $mailMessage = "Письмо не отправлено. Ошибка: {$mail->ErrorInfo}";
        }

//        header("Location: ".$_SERVER['REQUEST_URI']);
    }

?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form telegraph</title>
</head>
<body>
    <?php if (isset($mailSend) && $mailSend === true): ?>
        <div style="background: green"><?= $mailMessage ?></div>
    <?php elseif (isset($mailSend) && $mailSend === false): ?>
        <div style="background: red"><?= $mailMessage ?></div>
    <?php endif; ?>
    <form action="" method="post">
        <input type="text" name="author" placeholder="введите имя"><br>
        <input type="textarea" name="text" placeholder="введите текст"><br>
        <input type="text" name="email" placeholder="введите email"><br>
        <input type="submit" name="submit" value="Отправить">
    </form>
</body>
</html>