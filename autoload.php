<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';

function loadedEntities ($className)
{
    require_once ($_SERVER['DOCUMENT_ROOT']) . '/entities/' . $className . '.php';
}

spl_autoload_register('loadedEntities');
